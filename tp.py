import re, pickle
import pandas as pd
import string,time
import inflect
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import nltk
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
import re,string,unicodedata
from nltk.stem import PorterStemmer
import matplotlib.pyplot as plt


#plot et save l'accuracy et la loss du model
def plot_acc_loss(h):
    plt.plot(h.history['accuracy'])
    plt.plot(h.history['val_accuracy'])
    plt.title("LSTM Accuracy ")
    plt.ylabel("Accuracy")
    plt.xlabel("Epochs")
    plt.legend(['Train','Test'],loc='best')
    plt.savefig("lstmacc.png")
    plt.show()

    #plot et save la loss du lstm
    plt.plot(h.history['loss'])
    plt.plot(h.history['val_loss'])
    plt.title("LSTM Loss ")
    plt.ylabel("Loss")
    plt.xlabel("Epochs")
    plt.legend(['Train','Test'],loc='best')
    plt.savefig("lstmloss.png")
    plt.show()

# lecture du dataset
df = pd.read_csv("movie_data.csv")

# check si duplicats
num_duplicates = df.duplicated().sum()
print(f"the number of duplicates in the the data", format(num_duplicates))

# drop les duplicats
df.drop_duplicates(inplace =True)
# print combien il y a de review négative et positive on voit que le dataset est bien équilibré
print(df['sentiment'].value_counts())

# print le nombre de valeur manquante dans le dataset on voit que il n'en a pas
print(df.isnull().sum().sort_values())

#remove tout les balises html
def remove_html_tag(text):
    pattern = re.compile("<.*?>")
    return pattern.sub( r'', text)

df['review']=df['review'].apply(remove_html_tag)

#remove les hashtag présent
def remove_hashtag(text):
    pattern = re.compile("#(\w+)")
    return pattern.sub( r'', text)

df['review']=df['review'].apply(remove_hashtag)

#remove les mention
def remove_mention(text):
    pattern = re.compile("@(\w+)")
    return pattern.sub( r'', text)

df['review']=df['review'].apply(remove_mention)

exclude  = string.punctuation


#Realise de la correction sur les phrases mais non utilisé car prends beaucoup trop de temps 1 heure +
"""
from textblob import TextBlob
def correct_sentence_spelling(sentence):

    sentence = TextBlob(sentence)

    result = sentence.correct()
    return result

df['review']=df['review'].apply(correct_sentence_spelling)
"""

# fonction remplacent tout les nombre par le valeur en string, exemple: 100 -> cent
def replace_numbers_with_strings(text):
    p = inflect.engine()

    def replace(match):
        number = int(match.group())
        return p.number_to_words(number)

    pattern = r'\b\d+\b'
    result = re.sub(pattern, replace, text)
    return result

df['review'] = df['review'].apply(replace_numbers_with_strings)


#tout en lowercase
df['review']=df['review'].str.lower()


# un dictionnaire pour enlevé tout les utilisations d'accent dans la langue anglaise
mapping = {"ain't": "is not", "aren't": "are not","can't": "cannot",
           "'cause": "because", "could've": "could have", "couldn't": "could not",
           "didn't": "did not",  "doesn't": "does not", "don't": "do not", "hadn't": "had not",
           "hasn't": "has not", "haven't": "have not", "he'd": "he would","he'll": "he will",
           "he's": "he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will",
           "how's": "how is",  "I'd": "I would", "I'd've": "I would have", "I'll": "I will",
           "I'll've": "I will have","I'm": "I am", "I've": "I have", "i'd": "i would",
           "i'd've": "i would have", "i'll": "i will",  "i'll've": "i will have",
           "i'm": "i am", "i've": "i have", "isn't": "is not", "it'd": "it would",
           "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have",
           "it's": "it is", "let's": "let us", "ma'am": "madam", "mayn't": "may not",
           "might've": "might have","mightn't": "might not","mightn't've": "might not have",
           "must've": "must have", "mustn't": "must not", "mustn't've": "must not have",
           "needn't": "need not", "needn't've": "need not have","o'clock": "of the clock",
           "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not",
           "sha'n't": "shall not", "shan't've": "shall not have", "she'd": "she would",
           "she'd've": "she would have", "she'll": "she will", "she'll've": "she will have",
           "she's": "she is", "should've": "should have", "shouldn't": "should not",
           "shouldn't've": "should not have", "so've": "so have","so's": "so as", "this's": "this is",
           "that'd": "that would", "that'd've": "that would have", "that's": "that is",
           "there'd": "there would", "there'd've": "there would have", "there's": "there is",
           "here's": "here is","they'd": "they would", "they'd've": "they would have",
           "they'll": "they will", "they'll've": "they will have", "they're": "they are",
           "they've": "they have", "to've": "to have", "wasn't": "was not", "we'd": "we would",
           "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have",
           "we're": "we are", "we've": "we have", "weren't": "were not",
           "what'll": "what will", "what'll've": "what will have","what're": "what are",
           "what's": "what is", "what've": "what have", "when's": "when is", "when've": "when have",
           "where'd": "where did", "where's": "where is", "where've": "where have", "who'll": "who will",
           "who'll've": "who will have", "who's": "who is", "who've": "who have", "why's": "why is",
           "why've": "why have", "will've": "will have", "won't": "will not", "won't've": "will not have",
           "would've": "would have", "wouldn't": "would not", "wouldn't've": "would not have", 
           "y'all": "you all", "y'all'd": "you all would","y'all'd've": "you all would have",
           "y'all're": "you all are","y'all've": "you all have","you'd": "you would",
           "you'd've": "you would have", "you'll": "you will", "you'll've": "you will have",
           "you're": "you are", "you've": "you have" }

nltk.download('stopwords')
nltk.download('omw-1.4')
stop = stopwords.words('english')
wl = WordNetLemmatizer()

nltk.download('wordnet')

# enlève les émoji, les urls et applique de la lemmatization tout en enlevant les stopwords
def clean_emoji_urls(text):
    #remplace tout les valeur a l'intérieur du dictionnaire
    text = ' '.join([mapping[t] if t in mapping else t for t in text.split(" ")])
    #remove tout type d'emoji inutile
    emoji_clean= re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)
    text = emoji_clean.sub(r'',text)
    text = re.sub(r'\.(?=\S)', '. ',text) #add space apès un point
    text = "".join([word.lower() for word in text if word not in string.punctuation]) #remove ponctuation
    #applique la lemmatization et enlève les stopwords
    text = " ".join([wl.lemmatize(word) for word in text.split() if word not in stop and word.isalpha()])
    return text


df['review']=df['review'].apply(clean_emoji_urls)

nltk.download('punkt')

# Applique du stemming
def do_stemming(text):
    stemmer = PorterStemmer()
    text = " ".join([stemmer.stem(word) for word in text.split() if word.isalpha()])
    return text

df['review']=df['review'].apply(do_stemming)

#remove urls
def remove_url(text):
    re_url = re.compile('https?://\S+|www\.\S+')
    return re_url.sub('', text)

df['review']=df['review'].apply(remove_url)


#simple convertion des sentiments en binaire
def to_num(sent):
    if sent == "Positive":
        return 1
    else:
        return 0

df["sentiment"] = df["sentiment"].apply(to_num)

##################### TRAINING #############################


import tensorflow as tf
#from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Flatten,Embedding,Activation,Dropout,SpatialDropout1D,Bidirectional,LSTM,SimpleRNN
from tensorflow.keras.layers import Conv1D,MaxPooling1D,GlobalAveragePooling1D,GlobalMaxPooling1D
from sklearn.model_selection import train_test_split
from nltk.stem import WordNetLemmatizer
from keras.layers import LSTM
from tcn import TCN


# sépare dataset
X_train, X_test,y_train,y_test = train_test_split(df['review'],df['sentiment'],test_size=0.20,stratify= df['sentiment'],random_state=42)

# apprentissage du tokenizer sur le dataset
token = tf.keras.preprocessing.text.Tokenizer()
token.fit_on_texts(X_train)

# Convert text data to sequences of integers
train_sequences = token.texts_to_sequences(X_train)
valid_sequences = token.texts_to_sequences(X_test)

vocab_size = len(token.word_index)+1

maxlen = 100 # maximum length après padding

# padding
X_train = pad_sequences(train_sequences, maxlen=maxlen,padding = 'post')
X_test = pad_sequences(valid_sequences, maxlen=maxlen,padding = 'post')

#sauvegarde le tokenizer pour etre utilisé plutard pour la prediction
with open('tokenizer.pickle', 'wb') as savefile:
    pickle.dump(token, savefile)

vec_size = 300

lstm =Sequential()
# Embedding layer
lstm.add(Embedding(input_dim = vocab_size, output_dim = vec_size,input_length=maxlen,trainable = False))
#lstm
lstm.add(LSTM(200,dropout=0.20))
Dense(512, activation='relu'),
lstm.add(Dense(1,activation='sigmoid'))

lstm.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
lstm_history = lstm.fit(X_train,y_train,epochs=10,validation_data=(X_test,y_test))

plot_acc_loss(lstm_history)

score_lstm = lstm.evaluate(X_test,y_test)
print("Test loss :",score_lstm[0])
print("Test Accuracy :",score_lstm[1])

lstm.save("lstm_model.h5")


from tensorflow.keras.layers import Conv1D, GlobalMaxPooling1D
from keras.callbacks import EarlyStopping

vec_size = 300
# Reseau de convolution 1D
conv_model = Sequential()
conv_model.add(Embedding(input_dim=vocab_size, output_dim=vec_size, input_length=maxlen))
conv_model.add(Conv1D(256, 3, activation='relu', strides=1))
conv_model.add(Dropout(0.20))
conv_model.add(GlobalMaxPooling1D())
conv_model.add(Dense(units=1, activation='sigmoid'))
conv_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
early_stop = EarlyStopping(monitor='val_loss', patience=2, restore_best_weights=True)
history = conv_model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=3, callbacks=[early_stop])

# Réseau TCN
vec_size = 300
tcn_model = Sequential()
tcn_model.add(Embedding(input_dim=vocab_size, output_dim=vec_size, input_length=maxlen ,trainable = False))
tcn_model.add(TCN(256, 3, activation='relu'))
tcn_model.add(Dropout(0.20))
tcn_model.add(Dense(1, activation='sigmoid'))

# Compile the model
tcn_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
tcn_history = tcn_model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10)

tcn_model.save("tcn_model.h5")


vec_size = 200
#BILSTM not tested
bi_lstm =Sequential()
# Embedding layer
bi_lstm.add(Embedding(input_dim = vocab_size, output_dim = vec_size,input_length=maxlen,trainable = False))
#lstm
bi_lstm.add(Bidirectional(LSTM(200, dropout=0.2, recurrent_dropout=0.2)))
bi_lstm.add(Dense(1,activation='sigmoid'))

bi_lstm.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
bi_lstm_history = bi_lstm.fit(X_train,y_train,epochs=10,validation_data=(X_test,y_test))